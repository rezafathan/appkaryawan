<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');

		$this->load->view('welcome_message');
	}
		//http://localhost/appkaryawan/index.php/welcome/name/agus/
	public function name(){
		echo urldecode($this->uri->segment("3"));
	}
	public function cari(){
		$this->load->view('karyawan/cari');
	}
	public function cari_action(){
		$this->load->model('karyawan_model','karyawan');
		$nama = $this->input->post('nama');
		$data['records']=$this->karyawan->find_by_name($nama);
		$this->load->view('karyawan/cari',$data);
	}

}
