<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('divisi_model','divisi');	
		$this->load->model('karyawan_model','karyawan');
		$this->load->model('nota_model','nota');	
		$this->load->model('notadetail_model','detail');		
		if(!$this->ion_auth->logged_in()){
			redirect(base_url('login'));
		}
		if(!$this->ion_auth->is_admin()){
			redirect(base_url('login'));
		}		
	}
	public function index(){
		
		$data['records'] = $this->karyawan->find_all();
		log_message('DEBUG',$this->db->last_query());
		$this->load->view('karyawan/index',$data);
	}
	public function tambah(){
		$jabatan = array("Manajer"=>"Manajer","Supervisor"=>"Supervisor","Karyawan"=>"Karyawan");
		$data['jabatan']=$jabatan;
		$data['divisi']=$this->divisi->find_all();
		$this->load->view('karyawan/tambah',$data);
	}
	public function tambah_save(){
		$this->form_validation->set_rules('nama','Nama Karyawan','required');
		$this->form_validation->set_rules('email','Email Karyawan','required|valid_email');	
		$this->form_validation->set_rules('telpon','Telepon Karyawan','required');
		if($this->form_validation->run() == FALSE){
			$jabatan = array("Manajer"=>"Manajer","Supervisor"=>"Supervisor","Karyawan"=>"Karyawan");
			$data['jabatan']=$jabatan;
			$data['divisi']=$this->divisi->find_all();
			$this->load->view('karyawan/tambah',$data);
		}
		else{
			//upload file
			$config = array('upload_path'=>"./assets/uploads/",'allowed_types'=>"*",'overwrite'=>TRUE,'max_size'=>"2048000");
			$file_name = "";
			if($_FILES['foto']['name'] != ""){
			$this->load->library('upload', $config);
			if($this->upload->do_upload('foto')){
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name'];
			}
			else{
				$error = array('error'=>$this->upload->display_errors());
				print_r($error);
				exit;
			}
			}
			$data = array(
				'nama'=>$this->input->post('nama'),
				'email'=>$this->input->post('email'),
				'telpon'=>$this->input->post('telpon'),
				'jabatan'=>$this->input->post('jabatan'),
				'jenis_kelamin'=>$this->input->post('jenis_kelamin'),
				'iddivisi'=>$this->input->post('iddivisi'),
				'tgllahir'=>$this->input->post('tgllahir'),
				'foto'=>$file_name
				);
			$this->karyawan->insert($data);
			redirect(base_url('karyawan'));
		}
	}
	public function detail(){
		$id = $this->uri->segment(3);
		$data['karyawan'] = $this->karyawan->find_by_id($id);
		$this->load->view('karyawan/detail',$data);
	}
	public function edit(){
		$id = $this->uri->segment(3);
		$data['karyawan'] = $this->karyawan->find_by_id($id);
		$jabatan = array("Manajer"=>"Manajer","Supervisor"=>"Supervisor","Karyawan"=>"Karyawan");
		$data['jabatan']=$jabatan;
		$data['divisi']=$this->divisi->find_all();
		$this->load->view('karyawan/edit',$data);		
	}
	public function edit_save(){
		$id = $this->input->post('id');
		$this->form_validation->set_rules('nama','Nama Karyawan','required');
		$this->form_validation->set_rules('email','Email Karyawan','required|valid_email');	
		$this->form_validation->set_rules('telpon','Telepon Karyawan','required');	
		if($this->form_validation->run() == FALSE){
			$data['karyawan'] = $this->karyawan->find_by_id($id);
			$jabatan = array("Manajer"=>"Manajer","Supervisor"=>"Supervisor","Karyawan"=>"Karyawan");
			$data['jabatan']=$jabatan;
			$data['divisi']=$this->divisi->find_all();
			$this->load->view('karyawan/edit',$data);
		}
		else{
			//upload file
			$config = array('upload_path'=>"./assets/uploads/",'allowed_types'=>"*",'overwrite'=>TRUE,'max_size'=>"2048000");
			$file_name = "";
			if($_FILES['foto']['name'] != ""){
			$this->load->library('upload', $config);
			if($this->upload->do_upload('foto')){
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name'];
			}
			else{
				$error = array('error'=>$this->upload->display_errors());
				print_r($error);
				exit;
			}
			}
			else{
				$file_name = $this->input->post('fot_lama');
			}
			$data = array(
				'nama'=>$this->input->post('nama'),
				'email'=>$this->input->post('email'),
				'telpon'=>$this->input->post('telpon'),
				'email'=>$this->input->post('email'),
				'jabatan'=>$this->input->post('jabatan'),
				'jenis_kelamin'=>$this->input->post('jenis_kelamin'),
				'iddivisi'=>$this->input->post('iddivisi'),
				'tgllahir'=>$this->input->post('tgllahir'),
				'foto'=>$file_name
				);
			$this->karyawan->update($id,$data);
			redirect(base_url('karyawan'));
		}		
	}	
	public function hapus(){
		$id = $this->uri->segment(3);
		if($id){
			$this->karyawan->delete($id);
		}
		redirect(base_url('karyawan'));
	}
	
	public function surat() {
        $data['records'] = $this->karyawan->find_all();
        $this->load->view('karyawan/surat', $data);
    }

    public function surat_save() {
        $nota = array(
            'nomor' => $this->input->post('nomor'),
            'tanggal' => date('Y-m-d')
        );
        $this->db->trans_begin();
		$this->nota->insert($nota);
        $notaid = $this->db->insert_id();
        $total = 0;
        for ($i = 0; $i < count($_POST['karyawan']); $i++) {
            $karyawanid = $_POST['karyawan'][$i];
            $biaya = $_POST['biaya'][$i];
            $qty = $_POST['qty'][$i];
            $subtotal = $_POST['subtotal'][$i];
            $detail = array(
                'idnota' => $notaid,
                'karyawanid' => $karyawanid,
                'biaya' => $biaya,
                'qty' => $qty,
                'subtotal' => $subtotal
            );
            $total += $subtotal;
            $this->detail->insert($detail);
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        $data['nomor'] = $this->input->post('nomor');
        $data['tanggal'] = date('Y-m-d');
        $data['post'] = $_POST;
        $data['total'] = $total;
        $this->load->view("karyawan/nota", $data);
    }
}
