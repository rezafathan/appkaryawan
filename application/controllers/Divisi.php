<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('divisi_model','divisi');
		if(!$this->ion_auth->logged_in()){
			redirect(base_url('login'));
		}
		if(!$this->ion_auth->is_admin()){
			redirect(base_url('login'));
		}
	}
	public function index(){
		
		$data['records'] = $this->divisi->find_all();
		log_message('DEBUG',$this->db->last_query());
		$this->load->view('divisi/index',$data);
	}
	public function tambah(){
		$this->load->view('divisi/tambah');
	}
	public function tambah_save(){
		$this->form_validation->set_rules('kode','Kode Divisi','required');
		$this->form_validation->set_rules('nama','Nama Divisi','required');	
		if($this->form_validation->run() == FALSE){
			$this->load->view('divisi/tambah');
		}
		else{
			$data = array('kode'=>$this->input->post('kode'),'nama'=>$this->input->post('nama'));
			$this->divisi->insert($data);
			redirect(base_url('divisi'));
		}
	}
	public function detail(){
		$id = $this->uri->segment(3);
		$data['divisi'] = $this->divisi->find_by_id($id);
		$this->load->view('divisi/detail',$data);
	}
	public function edit(){
		$id = $this->uri->segment(3);
		$data['divisi'] = $this->divisi->find_by_id($id);
		$this->load->view('divisi/edit',$data);		
	}
	public function edit_save(){
		$id = $this->input->post('id');
		$this->form_validation->set_rules('kode','Kode Divisi','required');
		$this->form_validation->set_rules('nama','Nama Divisi','required');	
		if($this->form_validation->run() == FALSE){
			$data['divisi'] = $this->divisi->find_by_id($id);
			$this->load->view('divisi/edit',$data);
		}
		else{
			$data = array('kode'=>$this->input->post('kode'),'nama'=>$this->input->post('nama'));
			$this->divisi->update($id,$data);
			redirect(base_url('divisi'));
		}		
	}
	public function hapus(){
		$id = $this->uri->segment(3);
		if($id){
			$this->divisi->delete($id);
		}
		redirect(base_url('divisi'));
	}
}