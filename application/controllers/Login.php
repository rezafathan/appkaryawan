<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
	}
	
	function index(){
		$this->load->view('login');
	}
	public function masuk(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if($this->ion_auth->login($username,$password)){
			redirect(base_url('divisi'));
		}
		else{
			$data['message'] = $this->ion_auth->errors();
			$this->load->view('login','data');
		}
	}
	public function logout(){
		$this->ion_auth->logout();
		redirect(base_url('login'));
	}
}
