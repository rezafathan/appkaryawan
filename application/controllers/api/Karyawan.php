<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Karyawan extends REST_Controller{
	function __construct($config = 'rest'){
		parent::__construct($config);
		$this->load->model('Karyawan_model','karyawan');
	}
	//localhost/appkaryawan/index
	function index_get(){
		$data = $this->karyawan->find_all();
		if($data){
			$this->response($data);
		}
		$this->response(null, REST_Controller::HTTP_NO_CONTENT);
	}
	
	function pagination_get(){
		$data = [];
		$limit_per_page = 2;
		//http://localhost/appkaryawan/api/karyawan/pagination/0
		$start_index = $this->uri->segment(4) ? $this->uri->segment(4) :0;
		$total_record = $this->karyawan->get_total();
		if($total_record > 0){
			$data = $this->karyawan->pagination($limit_per_page,$start_index);
		}
		if($data){
			$this->response($data);
			$this->response(null, REST_Controller::HTTP_NO_CONTENT);
		}
	}
	public function insert_post(){
		$this->form_validation->set_rules('nama','Nama Karyawan','required');
		$this->form_validation->set_rules('email','Email Karyawan','required|valid_email');	
		$this->form_validation->set_rules('telpon','Telepon Karyawan','required');
		if($this->form_validation->run() == FALSE){
			$this->response(validation_errors(), REST_Controller::HTTP_BAD_REQUEST);
		}
		else{
			$data = array(
				'nama'=>$this->input->post('nama'),
				'email'=>$this->input->post('email'),
				'telpon'=>$this->input->post('telpon'),
				'jabatan'=>$this->input->post('jabatan'),
				'jenis_kelamin'=>$this->input->post('jenis_kelamin'),
				'iddivisi'=>$this->input->post('iddivisi'),
				'tgllahir'=>$this->input->post('tgllahir'),
				'foto'=>$this->input->post('foto')
				);
			$result = $this->karyawan->insert($data);
			$this->response($result);
		}
	}	
}