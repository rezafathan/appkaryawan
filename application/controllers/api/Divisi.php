<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Divisi extends REST_Controller{
	function __construct($config = 'rest'){
		parent::__construct($config);
		$this->load->model('Divisi_model','divisi');
		$token = $this->post('token');
		if(!$token){
			$token = $this->get('token');
		}
		if(!$token){
			$this->response('Access Denied', REST_Controller::HTTP_FORBIDDEN);
		}
		$this->load->model('users_model','users');
		$user = $this->users->find_by_token($token);
		if(!$user){
			$this->response('Invalid Token', REST_Controller::HTTP_FORBIDDEN);
		}
	}
	//localhost/appkaryawan/index
	function index_get(){
		$data = $this->divisi->find_all();
		if($data){
			$this->response($data);
		}
		$this->response(null, REST_Controller::HTTP_NO_CONTENT);
		
	}
	function pagination_get(){
		$data = [];
		$limit_per_page = 2;
		//http://localhost/appkaryawan/api/divisi/pagination/0
		$start_index = $this->uri->segment(4) ? $this->uri->segment(4) :0;
		$total_record = $this->divisi->get_total();
		if($total_record > 0){
			$data = $this->divisi->pagination($limit_per_page,$start_index);
		}
		if($data){
			$this->response($data);
			$this->response(null, REST_Controller::HTTP_NO_CONTENT);
		}
	}
	
	public function insert_post(){
		$this->form_validation->set_rules('kode','Kode Divisi','required');
		$this->form_validation->set_rules('nama','Nama Divisi','required');	
		if($this->form_validation->run() == FALSE){
			$this->response(validation_errors(), REST_Controller::HTTP_BAD_REQUEST);
		}
		else{
			$data = array('kode'=>$this->input->post('kode'),'nama'=>$this->input->post('nama'));
			$result = $this->divisi->insert($data);
			$this->response($result);
		}
	}
	public function detail_get(){
		$id = $this->uri->segment(4);
		$data = $this->divisi->find_by_id($id);
		if($data){
			$this->response($data);
		}
		$this->response(null, REST_Controller::HTTP_NO_CONTENT);
	}
	public function update_post(){
		$id = $this->input->post('id');
		$this->form_validation->set_rules('kode','Kode Divisi','required');
		$this->form_validation->set_rules('nama','Nama Divisi','required');	
		if($this->form_validation->run() == FALSE){
			$this->response(validation_errors(), REST_Controller::HTTP_BAD_REQUEST);
		}
		else{
			$data = array('kode'=>$this->input->post('kode'),'nama'=>$this->input->post('nama'));
			$result = $this->divisi->update($id,$data);
			$this->response($result);
		}
	}
	public function delete_post(){
		$id =$this->uri->segment(4);
		$result = $this->divisi->delete($id);
		$this->response($result);
	}
	
}