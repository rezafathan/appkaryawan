
<?php
	include APPPATH . 'views/fragment/header.php';
	include APPPATH . 'views/fragment/menu.php';
?>
<div class="break"></div>
<div class="bg-login">

<h2 class="login">Login</h2>
<?= isset($message) ? $message : "" ?>

<form method="post" action="<?= base_url('login/masuk') ?>">
	<div>
		<label>Email</label>
		<input type="text" name="username" required />
	</div>
	<div>
		<label>Password</label>
		<input type="password" name="password" required />
	</div>
	<div>
		<input type="submit" name="Login" class="btn btn-primary" value="LOGIN"/>
	</div>
</form>
</div>
<br>

<?php 
	include APPPATH . 'views/fragment/footer.php';
?>