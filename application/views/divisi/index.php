<?php
	include APPPATH . 'views/fragment/header.php';
	include APPPATH . 'views/fragment/menu.php';
?>
<h2>Data Divisi</h2>
<a href="<?= base_url('divisi/tambah') ?>" class="btn btn-success pull-right">Tambah</a>
<table class="table table-striped">
	<tr>
		<th>Kode</th>
		<th>Nama</th>
		<th>Aksi</th>
	</tr>
<?php
	foreach($records as $idx => $row){
?>
		<tr>
			<td><?= $row['kode']?></td>
			<td><?= $row['nama']?></td>
			<td>
				<a href="<?= base_url('divisi/detail') ?>/<?= $row['id']?>" class="btn btn-small btn-primary">Detail</a>
				<a href="<?= base_url('divisi/edit') ?>/<?= $row['id']?>" class="btn btn-small btn-warning">Edit</a>
				<a onclick="return confirm ('menghapus data')" href="<?= base_url('divisi/hapus') ?>/<?= $row['id']?>" class="btn btn-small btn-danger">Hapus</a>
			</td>
		</tr>
<?php
}
?>
</table>

<?php 
	include APPPATH . 'views/fragment/footer.php';
?>


<!-- print_r($records); -->