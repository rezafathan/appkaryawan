<?php
include APPPATH . 'views/fragment/header.php';
include APPPATH . 'views/fragment/menu.php';
?>
<script type="text/javascript">
	$(function(){
		$('input.datepicker').each(function(){
			var datepicker = $(this);
			datepicker.bootstrapDatePicker(datepicker.data());
	});
	});

</script>
<h3>Tambah Karyawan</h3>
<?php echo validation_errors();?>
<form enctype="multipart/form-data" action="<?= base_url('karyawan/tambah_save')?>" method="post">
<div>
<label>Nama:</label>
<input type="text" name="nama" required />
</div>
<div>
<label>Email:</label>
<input type="text" name="email" required />
</div>
<div>
<label>Telepon:</label>
<input type="text" name="telpon" required />
</div>
<div>
<label>Jabatan:</label>
<select name="jabatan">
<?php 
foreach ($jabatan as $key => $label){
	?>
	<option value="<?= $key ?>"><?= $label ?></option>
	<?php
}
?>
</select>
</div>
<div>
<div>
<label>Jenis Kelamin:</label>
<input type="radio" name="jenis_kelamin" value="L" />L
<input type="radio" name="jenis_kelamin" value="P" />P
</div>
<div>
<label>Divisi:</label>
<select name="iddivisi">
<?php 
foreach ($divisi as $key => $row){
	?>
	<option value="<?= $row['id'] ?>"><?= $row['nama'] ?></option>
	<?php
}
?>
</select>
</div>
<div style="width:300px">
<label>Tanggal Lahir:</label>
<input type="text" name="tgllahir" required  id="tgllahir" data-default-today="true" data-date-format="DD-MM-YYYY" class="form-control datepicker col-sm-4"/>
</div>
<div>
<label>Foto:</label>
<input type="file" name="foto" required />
</div>
<input type="submit" name="Simpan"/>
</div>
</form>

<?php 
include APPPATH . 'views/fragment/footer.php';
?>