<?php
include APPPATH . 'views/fragment/header.php';
include APPPATH . 'views/fragment/menu.php';
?>
<script type="text/javascript">
	$(function(){
		$('input.datepicker').each(function(){
			var datepicker = $(this);
			datepicker.bootstrapDatePicker(datepicker.data());
	});
	});

</script>
<h3>Edit Karyawan</h3>
<?php echo validation_errors();?>
<form enctype="multipart/form-data" action="<?= base_url('karyawan/edit_save')?>" method="post">
<input type="hidden" value="<?= $karyawan['id']?>" name="id" required />
<input type="hidden" value="<?= $karyawan['foto']?>" name="foto_lama" />
<div>
<label>Kode:</label>
<input type="text" value="<?= $karyawan['nama']?>" name="nama" required />
</div>
<div>
<label>Nama:</label>
<input type="text" value="<?= $karyawan['email']?>"name="email" required />
</div>
<div>
<label>Telepon:</label>
<input type="text" value="<?= $karyawan['telpon']?>" name="telpon" required />
</div>
<div>
<label>Jabatan:</label>
<select name="jabatan">
<?php 
foreach ($jabatan as $key => $label){
	$selected = "";
	if ($key == $karyawan['jabatan']){
		$selected = "selected";
	}	
	?>
	<option value="<?= $key ?>" <?= $selected ?>><?= $label ?></option>
	<?php
}
?>
</select>
</div>
<div>
<div>
<label>Jenis Kelamin:</label>
<?php
	$checkedL = "";
	$checkedP = "";
	if ($karyawan['jenis_kelamin']=="L"){
		$checkedL = "checked";
		$checkedP = "";		
	}
	else{
		$checkedL = "";
		$checkedP = "checked";
	}
?>
<input type="radio" name="jenis_kelamin" value="L" <?= $checkedL ?>/>L
<input type="radio" name="jenis_kelamin" value="P" <?= $checkedP ?>/>P
</div>
<div>
<label>Divisi:</label>
<select name="iddivisi">
<?php 
foreach ($divisi as $key => $row){
	$selected = "";
	if ($row['id'] == $karyawan['iddivisi']){
		$selected = "selected";
	}
	?>
	<option value="<?= $row['id'] ?>" <?= $selected ?>><?= $row['nama'] ?></option>
	<?php
}
?>
</select>
</div>
<div style="width:300px">
<label>Tanggal Lahir:</label>
<input type="text" name="tgllahir" required  id="tgllahir" data-default-today="true" data-date-format="DD-MM-YYYY" class="form-control datepicker col-sm-4"  value="<?= $karyawan['tgllahir'] ?>" required />
</div>
<img src="<?= BASE_ASSETS .'/uploads/'. $karyawan['foto'] ?>" style="width:100px">
<div>
<label>Upload Foto:</label>
<input type="file" name="foto" required />
</div>
<div>
<input type="submit" name="Simpan"/>
</div>
</form>

<?php 
include APPPATH . 'views/fragment/footer.php';
?>