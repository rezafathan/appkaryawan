<?php
	include APPPATH . 'views/fragment/header.php';
	include APPPATH . 'views/fragment/menu.php';
?>
<h2>Data Karyawan</h2>
<a href="<?= base_url('karyawan/tambah') ?>" class="btn btn-success pull-right">Tambah</a>
<table class="table table-striped">
	<tr>
		<th>Nama</th>
		<th>Email</th>
		<th>Telepon</th>
		<th>Jabatan</th>
		<th>Jenis Kelamin</th>
		<th>Divisi</th>
		<th>Tanggal Lahir</th>
		<th>Foto</th>
		<th>Action</th>
	</tr>
<?php
	foreach($records as $idx => $row){
?>
		<tr>
			<td><?= $row['nama']?></td>
			<td><?= $row['email']?></td>
			<td><?= $row['telpon']?></td>
			<td><?= $row['jabatan']?></td>
			<td><?= $row['jenis_kelamin']?></td>
			<td><?= $row['namadivisi']?></td>
			<td><?= $row['tgllahir']?></td>
			<td><img src="<?= BASE_ASSETS .'/uploads/'. $row['foto'] ?>" style="height:10%"></td>
			<td>
				<a href="<?= base_url('karyawan/detail') ?>/<?= $row['id']?>" class="btn btn-small btn-primary">Detail</a>
				<a href="<?= base_url('karyawan/edit') ?>/<?= $row['id']?>" class="btn btn-small btn-warning">Edit</a>
				<a onclick="return confirm ('menghapus data')" href="<?= base_url('karyawan/hapus') ?>/<?= $row['id']?>" class="btn btn-small btn-danger">Hapus</a>
			</td>
			
		</tr>
<?php
}
?>
</table>

<?php 
	include APPPATH . 'views/fragment/footer.php';
?>


<!-- print_r($records); -->