<?php
include APPPATH . 'views/fragment/header.php';
include APPPATH . 'views/fragment/menu.php';
?>

<h3>Pencarian Data Karyawan</h3>
<form method="post" action="<?=base_url('welcome/cari_action') ?>">
	<div>
		<input type="text" name="nama" placeholder="Cari nama Karyawan..."/>
		<input type="submit" value="Cari" />
	</div>
</form>

<table class="table table-striped">
	<tr>
		<th>Nama</th>
		<th>Email</th>
		<th>Telepon</th>
		<th>Jabatan</th>
		<th>Jenis Kelamin</th>
		<th>Divisi</th>
		<th>Tanggal Lahir</th>
	</tr>
<?php
if(isset($records)){
	foreach($records as $idx => $row){
?>
		<tr>
			<td><?= $row['nama']?></td>
			<td><?= $row['email']?></td>
			<td><?= $row['telpon']?></td>
			<td><?= $row['jabatan']?></td>
			<td><?= $row['jenis_kelamin']?></td>
			<td><?= $row['namadivisi']?></td>
			<td><?= $row['tgllahir']?></td>
			
		</tr>
<?php
	}
}
?>
</table>
<?php 
include APPPATH . 'views/fragment/footer.php';
?>