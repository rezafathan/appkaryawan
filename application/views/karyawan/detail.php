<?php
	include APPPATH . 'views/fragment/header.php';
	include APPPATH . 'views/fragment/menu.php';
?>
<h3>Detail Karyawan</h3>
<table class="table table-striped">
<tr>
<th>Nama Karyawan</th>
<td><?= $karyawan['nama'] ?></td>
</tr>
<tr>
<th>Tanggal Lahir</th>
<td><?= $karyawan['tgllahir'] ?></td>
</tr>
<tr>
<th>Email Karyawan</th>
<td><?= $karyawan['email'] ?></td>
</tr>
<tr>
<th>Telepon Karyawan</th>
<td><?= $karyawan['telpon'] ?></td>
</tr>
<tr>
<th>Jabatan Karyawan</th>
<td><?= $karyawan['jabatan'] ?></td>
</tr>
<tr>
<th>Jenis Kelamin Karyawan</th>
<td><?= $karyawan['jenis_kelamin'] ?></td>
</tr>
<tr>
<th>Divisi Karyawan</th>
<td><?= $karyawan['namadivisi'] ?></td>
</tr>
<tr>
<th>Foto</th>
<td><img src="<?= BASE_ASSETS .'/uploads/'. $karyawan['foto'] ?>" style="height:20%"></td>
</tr>
</table>
<?php 
	include APPPATH . 'views/fragment/footer.php';
?>